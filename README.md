# client-store

> Simple client side state management store for any SPA framework

- Intuitive
- Immutable states
- Very Light Weight

client-store will work with any framework.

## Installation

**add the registry to your projects .npmrc file**  
1. `@sameke-oss:registry=https://gitlab.com/api/v4/projects/53081603/packages/npm/`
2. `npm install @sameke-oss/client-store`

## Usage

```typescript
import { Store } from '@sameke-oss/client-store';

let store = new Store();

// REGISTER THE STORE WITH YOUR CHOSEN FRAMEWORKS METHOD OF DEPENDENCY INJECTION

// add any states you wish to track (object, primitives, etc)
store.addState('userName', 'user1234');


// ovserve added state for changes
let userNameSignal = store.getState('userName');
let subscription = userNameSignal.subscribe(() => {
    console.log('user name has changed');
});

// WARNING: DON'T FORGET TO UNSUBSCRIBE FROM SIGNALS ON PAGE NAVIGATION
userNameSignal.unsubscribe(subscription);

```



