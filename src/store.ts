import { IStateOptions } from "./i-state-options";
import { Signal } from '@sameke-oss/simple-signals';

type ITracked<T> = {
    key: string;
    value: Signal<T>;
};

/**
 * Store for managing application state
 */
export class Store {
    private _observables: Array<ITracked<any>> = [];

    /**
     * Will add a new state with given initial value and options.
     * If state with the same key already exists then it will return existing state signal without modification.
     * @param key key for the tracked state value
     * @param initialValue initial value for the state
     * @param options options for the state
     * @returns a signal that can be used to get/set/observe the state
     */
    public addState<T>(key: string, initialValue?: T, options: IStateOptions = null): Signal<T> {
        let found = this._observables.find(o => o.key === key);
        if (found == null) {
            let newState = new Signal<T>(initialValue, options);
            let tracked: ITracked<T> = {
                key: key,
                value: newState
            };
            this._observables.push(tracked);
            found = tracked;
        } else {
            console.trace(`State with key ${key} already exists. Not modifying existing state registration.`);
        }
        return found.value;
    }

    public getState<T>(key: string): Signal<T> {
        let found = this._observables.find(o => o.key === key);
        if (found == null) {
            let newState = new Signal<T>(null);
            let tracked: ITracked<T> = {
                key: key,
                value: newState
            };
            this._observables.push(tracked);
            found = tracked;
        }

        return found.value;
    }
}