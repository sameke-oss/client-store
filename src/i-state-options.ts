import { ISignalOptions } from '@sameke-oss/simple-signals';
export { Predicate } from '@sameke-oss/simple-signals';
export type IStateOptions = ISignalOptions;