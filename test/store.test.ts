import { describe } from 'mocha';
import { Store } from '../src';
import { expect } from 'chai';

describe('Store', () => {
    it('should add a state', () => {
        let store = new Store();
        store.addState('test', 'test');

        let state = store.getState('test');
        expect(state).to.not.be.null;
        expect(state.value).to.equal('test');
    });

    it('should allow monitoring a state', (done) => {
        let store = new Store();
        store.addState('test', 'test');

        let state = store.getState('test');
        let timeout: any = null;
        state.subscribe(() => {
            clearTimeout(timeout);
            expect(state.value).to.equal('test2');
            done();
        });

        timeout = setTimeout(() => {
            expect.fail('timeout reached');
            done();
        }, 100);

        state.value = 'test2';
    });
});